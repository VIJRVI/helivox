
var width = window.innerWidth;
var tit = "";

var databaseLink = "https://helivox-361801.uc.r.appspot.com"


currentComments = {};

let stemTopics = ["None" , "Science", "Math", "Med", "CS", "Technology", "Standardized Testing"];
let sportsTopics = ["None" , "Boys", "Girls", "Winter", "Spring", "Fall", "Dance", "Swim", "Personal Fitness", "Self Defense"];
let artsTopics = ["None" , "Art", "Lit", "Pub. Speaking", "Lang/Culture", "Drama", "Music", "Film"];
let miscTopics = ["None" , "Business", "Volunteering", "Religion", "Social Studies", "Life Skills", "Trade-Specific", "Trivia"];

var cardData;

var cardDtaFull;

var currentTab = "stem"

var currentSort = ["Hours", "Cost", "Tags"]


// Sets active panel onload

var prevTab = document.getElementById("stem");
var prevTab1 = document.getElementById("STEM");
prevTab.setAttribute("class", "nav-link active");
prevTab1.setAttribute("class", "tab-pane active");



// Builds main system

let firstPromise = fetch(databaseLink + "/getcourse");
let secondPromise = firstPromise.then(response => response.json());
secondPromise.then(cardDtaFromBackend => {
    document.getElementById("loader").remove()


    cardDtaFull = cardDtaFromBackend


        

});

window.onload = function() {
    if(window.innerWidth <= 990){
        let log_in = document.createElement("li")
        log_in.setAttribute("class", "nav-item")
        let log_inner = document.createElement("a")
        log_inner.setAttribute("href", "login.html")
        log_inner.setAttribute("class", "nav-link")
        let log_content = document.createElement("span")
        log_content.setAttribute("class", "nav-link-inner--text")
        if(localStorage.getItem("name") != null){log_content.innerText = "Logged in"}
        else {log_content.innerText = "Login"}
        
        log_inner.appendChild(log_content)
        log_in.appendChild(log_inner)
        document.getElementById("nav-location").appendChild(log_in)
    }
    if(localStorage.getItem("name") != null){
        document.getElementById("login-val").innerText = localStorage.getItem("name")
    }
    if(localStorage.getItem("admin") != null){
        document.getElementById("login-val").innerText = "Admin"
    }
};


// Build one modal - reusable
buildModal();


phoneFormat();    


    

// METHODS



// Called when user clicks on modal span, defines what modal is being used, and displays modal

function mdlBlock(x) {
    modal.style.display = "block";
    tit = x;

    //code to add info to modal

    comments = findByTitle(x)["comments"]

    for(let t = 0; t < comments.length; t++){
        var li = document.createElement("li");
        var text = document.createTextNode(comments[t]);
        li.appendChild(text);
        li.setAttribute("class", "comment-val");
        document.getElementById("unordered").appendChild(li);
    }
    if(currentComments[x] != undefined){
        var li = document.createElement("li");
        var text = document.createTextNode(currentComments[x]);
        li.appendChild(text);
        li.setAttribute("class", "comment-val");
        document.getElementById("unordered").appendChild(li);
    }

  

}

function findByTitle(nam){
    for(let u = 0; u < cardData.length; u++){
        if(cardData[u]["title"] === nam){return cardData[u]}
    }
}


function phoneFormat(){
    const leftS = document.getElementById("leftstemformat");
    const rightS = document.getElementById("rightstemformat");
    const leftSp = document.getElementById("leftsportsformat");
    const rightSp = document.getElementById("rightsportsformat");
    const leftA = document.getElementById("leftartsformat");
    const rightA = document.getElementById("rightartsformat");
    const leftM = document.getElementById("leftmiscformat");
    const rightM = document.getElementById("rightmiscformat");

    if(width < 1530 && width > 992){
        leftS.setAttribute("class", "col-lg-12");
        rightS.setAttribute("class", "col-lg-12");
        leftSp.setAttribute("class", "col-lg-12");
        rightSp.setAttribute("class", "col-lg-12");
        leftA.setAttribute("class", "col-lg-12");
        rightA.setAttribute("class", "col-lg-12");
        leftM.setAttribute("class", "col-lg-12");
        rightM.setAttribute("class", "col-lg-12");
    }

}



function setDefaults(){

    document.getElementById("tagbutton").innerText = "Tags";
    document.getElementById("hrbutton").innerText = "Hours";
    document.getElementById("costbutton").innerText = "Cost";
    resetAll("cst", 5);
    resetAll("hrs", 5);

    currentSort = ["Hours", "Cost", "Tags"];

    resetTags();
}

function loadDOM(cardDta){

    var linkNums = 0;
    var stemcount = 0;
    var sportscount = 0;
    var artscount = 0;
    var misccount = 0;

    for(let i = 0; i < cardDta.length; i++){
        //console.log(cardDta[i]);


        // Define location to href for each element
        let location;
        if(cardDta[i]["category"] === "stem"){
            if(stemcount % 2 === 0){
                location = "left" + cardDta[i]["category"];
            }
            else{
                location = "right" + cardDta[i]["category"];
            }
            stemcount++;
        }
        else if(cardDta[i]["category"] === "sports"){
            if(sportscount % 2 === 0){
                location = "left" + cardDta[i]["category"];
            }
            else{
                location = "right" + cardDta[i]["category"];
            }
            sportscount++;
        }
        else if(cardDta[i]["category"] === "arts"){
            if(artscount % 2 === 0){
                location = "left" + cardDta[i]["category"];
            }
            else{
                location = "right" + cardDta[i]["category"];
            }
            artscount++;
        }
        else if(cardDta[i]["category"] === "misc"){
            if(misccount % 2 === 0){
                location = "left" + cardDta[i]["category"];
            }
            else{
                location = "right" + cardDta[i]["category"];
            }
            misccount++;
        }
        let element = document.getElementById(location);







        let main = document.createElement("div");
        main.classList.add("my-header", "card-header");
        main.setAttribute("id", cardDta[i]["id"]);

        let title = document.createElement("h4");
        title.setAttribute("class", "testboot");

        let clickableTitle = document.createElement("a");
        if(cardDta[i]["link"].trim() != "na"){ 
            clickableTitle.setAttribute("href", cardDta[i]["link"]);
            clickableTitle.setAttribute("target", "_blank");
        }
        clickableTitle.innerText = cardDta[i]["title"] + " ";
        title.appendChild(clickableTitle);

        

        //add following element into "title"


        

        // tags

        let tagsHold = cardDta[i]["tags"]

        let tagHold = document.createElement("div");

        let commentsHold = document.createElement("a");
        commentsHold.setAttribute("onclick", "mdlBlock(\"" + cardDta[i]["title"] + "\")");
        let comments = document.createElement("span");
        comments.classList.add("badge", "badge-pill" , "badge-info");
        comments.innerText = "Comments";
        commentsHold.appendChild(comments);

        tagHold.appendChild(commentsHold);

        for(let w = 0; w < tagsHold.length; w++){
            
            let tag = document.createElement("span");
            tag.classList.add("badge", "badge-pill" , "badge-primary");
            tag.innerText = tagsHold[w];
            tagHold.appendChild(tag);
        }




        // hrs commitment
        let hr = document.createElement("span");
        hr.classList.add("badge", "badge-pill" , "badge-secondary");
        hr.innerText = cardDta[i]["hrsCommit"] + " hrs/week";
        tagHold.appendChild(hr);
    

        //cost
        let cos = document.createElement("span");
        cos.classList.add("badge", "badge-pill" , "badge-success");
        cos.innerText = "$" + cardDta[i]["cost"];
        tagHold.appendChild(cos);

  

        title.appendChild(tagHold);




        main.appendChild(title);



        let innermain = document.createElement("div");
        innermain.setAttribute("class", "row");



        let innermain2 = document.createElement("div");
        innermain2.classList.add("col-lg-4","col-md-6" );
        if(width > 770 && width < 993) innermain2.classList.add("col-lg-4","col-md-12" );
        innermain.appendChild(innermain2);




        //list in innermain2

        let ul = document.createElement("ul");
        ul.classList.add("nav", "nav-pills", "nav-pills-rose", "nav-pills-icons", "flex-column");
        ul.setAttribute("role", "tablist");
        innermain2.appendChild(ul);

        let listItem1 = document.createElement("li");
        listItem1.setAttribute("class", "nav-item");
        ul.appendChild(listItem1);

        let link1 = document.createElement("a");
        link1.classList.add("nav-link", "active");
        link1.setAttribute("data-toggle", "tab");
        link1.setAttribute("href", "#link" + linkNums);
        link1.setAttribute("role", "tablist");
        link1.innerText = "Image";
        listItem1.appendChild(link1);

        let icon1 = document.createElement("i");
        icon1.setAttribute("class", "material-icons");
        icon1.innerText = "dashboard";
        link1.appendChild(icon1);

        let listItem2 = document.createElement("li");
        listItem2.setAttribute("class", "nav-item");
        ul.appendChild(listItem2);

        let link2 = document.createElement("a");
        link2.classList.add("nav-link");
        link2.setAttribute("data-toggle", "tab");
        link2.setAttribute("href", "#link" + (linkNums+1));
        link2.setAttribute("role", "tablist");
        link2.innerText = "Description";
        listItem2.appendChild(link2);

        let icon2 = document.createElement("i");
        icon2.setAttribute("class", "material-icons");
        icon2.innerText = "schedule";
        link2.appendChild(icon2);


        // innermain3

        let innermain3 = document.createElement("div");
        innermain3.setAttribute("class", "col-md-8");
        if(width > 992 && width < 1528) innermain3.setAttribute("class", "col-md-6");
        if(width > 770 && width < 993) innermain3.setAttribute("class", "col-md-12");
        
        innermain.appendChild(innermain3);

        let tabpane = document.createElement("div");
        tabpane.setAttribute("class", "tab-content");
        innermain3.appendChild(tabpane);


        let contentcontainer = document.createElement("div");
        contentcontainer.classList.add("tab-pane", "active");
        contentcontainer.setAttribute("id" , "link" + linkNums);
        tabpane.appendChild(contentcontainer);

        let image = document.createElement("img");
        image.setAttribute("src", cardDta[i]["image"]);
        image.setAttribute("class", "imgstd");
        image.setAttribute("alt", "Extracurriculars");

        contentcontainer.appendChild(image);

        let contentcontainer2 = document.createElement("div");
        contentcontainer2.setAttribute("class", "tab-pane");
        contentcontainer2.setAttribute("id", "link" + (linkNums+1));
        let container2P = document.createElement("p");
        container2P.innerText = cardDta[i]["description"];
        container2P.setAttribute("class" , "pclass");
        contentcontainer2.appendChild(container2P);








        tabpane.appendChild(contentcontainer2);




        main.appendChild(innermain);

        //appends to the main webpage
        element.appendChild(main);

        linkNums+=2;

    }

}

async function postData(url = '', data = {}) {
    // Default options are marked with *
    const response = await fetch(url, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        headers: {
        'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: JSON.stringify(data) // body data type must match "Content-Type" header
    });
    
        return response; // parses JSON response into native JavaScript objects
}

function changeTag(topic){
    setDefaults();


    var filterTag = document.getElementById("filter");


    filterTag.innerHTML = "";
    if(topic === "stem"){

    for(let c = 0; c < stemTopics.length; c++){
        let sc = document.createElement("a");
        sc.innerText = stemTopics[c];
        sc.setAttribute("id", "tg" + c);
        filterTag.appendChild(sc);
    }
    }
    else if(topic === "sports"){
    for(let c = 0; c < sportsTopics.length; c++){
        let sc = document.createElement("a");
        sc.innerText = sportsTopics[c];
        sc.setAttribute("id", "tg" + c);
        filterTag.appendChild(sc);

    }

    }
    else if(topic === "arts"){
    for(let c = 0; c < artsTopics.length; c++){
        let sc = document.createElement("a");
        sc.innerText = artsTopics[c];
        sc.setAttribute("id", "tg" + c);
        filterTag.appendChild(sc);
    }
    }
    else if(topic === "misc"){
    for(let c = 0; c < miscTopics.length; c++){
        let sc = document.createElement("a");
        sc.innerText = miscTopics[c];
        sc.setAttribute("id", "tg" + c);
        filterTag.appendChild(sc);
    }
    }





}


function buildModal(){
    
    let myModal = document.createElement("div");
    myModal.setAttribute("id", "myModal");
    myModal.setAttribute("class", "modal1");

    let modalContent = document.createElement("div");
    modalContent.setAttribute("class", "modal1-content");


    let spanContent = document.createElement("span");
    spanContent.setAttribute("class", "close");
    spanContent.innerHTML = "&times;";
    modalContent.appendChild(spanContent);


    let container1 = document.createElement("div");
    container1.setAttribute("class", "container1");

    if(width <= 700 ){
        container1.setAttribute("style", " margin: 0px; width: 100%;");
    }

    let commen = document.createElement("div");
    commen.setAttribute("class", "comment43");

    let comments43 = document.createElement("ul");
    comments43.setAttribute("class", "comments43");
    comments43.setAttribute("id", "unordered");
    commen.appendChild(comments43);
    container1.appendChild(commen);


    let pWarn = document.createElement("p");
    pWarn.setAttribute("style", "font-weight: bold")
    pWarn.innerHTML = "*You can only comment once";
    modalContent.appendChild(pWarn);

    let commentBox = document.createElement("input");
    commentBox.setAttribute("type", "text");
    commentBox.setAttribute("id", "comment-box");
    commentBox.setAttribute("placeholder", "Enter Comment");
    container1.appendChild(commentBox);

    let commentButton = document.createElement("button");
    commentButton.setAttribute("id", "post");
    commentButton.setAttribute("class", "comment-button");
    commentButton.innerHTML = "Comment";
    container1.appendChild(commentButton);




    modalContent.appendChild(container1);
    myModal.appendChild(modalContent);
    document.body.appendChild(myModal);

}

// Resets color on tags

function resetAll(name, num){
    for(let i = 0; i  < num; i++){
        document.getElementById(name+ i).setAttribute("style", "background-color: #fffcfc")
    }
}


// Hides cards that dont match criteria

function filterByTag(sortable){
    resetTags();
    for(let r = 0; r < cardData.length; r++){

        // Hours
        if(sortable[0] != "Hours"){
            if(sortable[0] === "0"){
                if(cardData[r]["hrsCommit"] != 0){
                    document.getElementById(cardData[r]["id"]).style.display = "none";
                }
                
            }
            else if(sortable[0].includes("-")){
                if(!(cardData[r]["hrsCommit"] >= sortable[0].split("-")[0] && cardData[r]["hrsCommit"] <= sortable[0].split("-")[1])){
                    document.getElementById(cardData[r]["id"]).style.display = "none";
                }
                
            }
            else {
                if(cardData[r]["hrsCommit"] < 10){
                    document.getElementById(cardData[r]["id"]).style.display = "none";
                }
                
            }
        }

        // Cost
        if(sortable[1] != "Cost"){
            if(sortable[1] === "0"){
                if(cardData[r]["cost"] != 0){
                    document.getElementById(cardData[r]["id"]).style.display = "none";
                }
                
            }
            else if(sortable[1].includes("-")){
                if(!(cardData[r]["cost"] >= sortable[1].split("-")[0] && cardData[r]["cost"] <= sortable[1].split("-")[1])){
                    document.getElementById(cardData[r]["id"]).style.display = "none";
                }
                
            }
            else {
                if(cardData[r]["cost"] < 50){
                    document.getElementById(cardData[r]["id"]).style.display = "none";
                }
                
            }
        }

        // Tags

        if(sortable[2] != "Tags"){
           if(!(cardData[r]["tags"].includes(sortable[2]))){
                document.getElementById(cardData[r]["id"]).style.display = "none";
           }
        }
    }


}


// Displays all hidden cards

function resetTags(){
    for(let r = 0; r < cardData.length; r++){
        document.getElementById(cardData[r]["id"]).style.display = "block";
    }
}





// Event Listeners



// TAGS STUFF

// Changes tags based on location 

var stemList = document.getElementById("stem");
stemList.addEventListener('click', function(){
    changeTag("stem");
    currentTab = "stem"
    resetAll("tg", 7)
});
var spoList = document.getElementById("sports");
spoList.addEventListener('click', function(){
    changeTag("sports");
    currentTab = "sports"
    resetAll("tg", 10)
});
var artList = document.getElementById("arts");
artList.addEventListener('click', function(){
    changeTag("arts");
    currentTab = "arts"
    resetAll("tg", 8)
});
var miscList = document.getElementById("misc");
miscList.addEventListener('click', function(){
    changeTag("misc");
    currentTab = "misc"
    resetAll("tg", 8)
});




// Listens for sort button click, and creates events for each tag



document.getElementById("tagbutton").addEventListener('mouseover', function(){
    if(currentTab === "stem"){
        var tgbutton = document.getElementById("tagbutton");
        document.getElementById("tg0").addEventListener('click', function(){
            tgbutton.innerText = "Tags";

            // Changes color for ease, resetAll() removes previous color
            resetAll("tg", 7);
            document.getElementById("tg0").setAttribute("style", "background-color: #e0dcdc");
            currentSort[2] = "Tags";

            filterByTag(currentSort);
            
        });
        document.getElementById("tg1").addEventListener('click', function(){
            tgbutton.innerText = "Science";
            resetAll("tg", 7)
            document.getElementById("tg1").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Science";
            filterByTag(currentSort);


        });
        document.getElementById("tg2").addEventListener('click', function(){
            tgbutton.innerText = "Med";
            resetAll("tg", 7)
            document.getElementById("tg2").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Med";
            filterByTag(currentSort);

        });
        document.getElementById("tg3").addEventListener('click', function(){
            tgbutton.innerText = "Math";
            resetAll("tg", 7)
            document.getElementById("tg3").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Math";
            filterByTag(currentSort);

        });
        document.getElementById("tg4").addEventListener('click', function(){
            tgbutton.innerText = "CS";
            resetAll("tg", 7)
            document.getElementById("tg4").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "CS";
            filterByTag(currentSort);

        });
        document.getElementById("tg5").addEventListener('click', function(){
            tgbutton.innerText = "Technology";
            resetAll("tg", 7)
            document.getElementById("tg5").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Technology";
            filterByTag(currentSort);

        });
        document.getElementById("tg6").addEventListener('click', function(){
            tgbutton.innerText = "Standardized Testing";
            resetAll("tg", 7)
            document.getElementById("tg6").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Standardized Testing";
            filterByTag(currentSort);

        });
    }
    if(currentTab === "sports"){
        var tgbutton = document.getElementById("tagbutton");
        document.getElementById("tg0").addEventListener('click', function(){
            tgbutton.innerText = "Tags";
            resetAll("tg", 10)
            document.getElementById("tg0").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Tags";
            filterByTag(currentSort);

        });
        document.getElementById("tg1").addEventListener('click', function(){
            tgbutton.innerText = "Boys";
            resetAll("tg", 10)
            document.getElementById("tg1").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Boys";
            filterByTag(currentSort);

        });
        document.getElementById("tg2").addEventListener('click', function(){
            tgbutton.innerText = "Girls";
            resetAll("tg", 10)
            document.getElementById("tg2").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Girls";
            filterByTag(currentSort);

        });
        document.getElementById("tg3").addEventListener('click', function(){
            tgbutton.innerText = "Winter";
            resetAll("tg", 10)
            document.getElementById("tg3").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Winter";
            filterByTag(currentSort);

        });
        document.getElementById("tg4").addEventListener('click', function(){
            tgbutton.innerText = "Spring";
            resetAll("tg", 10)
            document.getElementById("tg4").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Spring";
            filterByTag(currentSort);

        });
        document.getElementById("tg5").addEventListener('click', function(){
            tgbutton.innerText = "Fall";
            resetAll("tg", 10)
            document.getElementById("tg5").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Fall";
            filterByTag(currentSort);

        });
        document.getElementById("tg6").addEventListener('click', function(){
            tgbutton.innerText = "Dance";
            resetAll("tg", 10)
            document.getElementById("tg6").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Dance";
            filterByTag(currentSort);

        });
        document.getElementById("tg7").addEventListener('click', function(){
            tgbutton.innerText = "Swim";
            resetAll("tg", 10)
            document.getElementById("tg7").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Swim";
            filterByTag(currentSort);

        });
        document.getElementById("tg8").addEventListener('click', function(){
            tgbutton.innerText = "Personal Fitness";
            resetAll("tg", 10)
            document.getElementById("tg8").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Personal Fitness";
            filterByTag(currentSort);

        });
        document.getElementById("tg9").addEventListener('click', function(){
            tgbutton.innerText = "Self Defense";
            resetAll("tg", 10)
            document.getElementById("tg9").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Self Defense";
            filterByTag(currentSort);

        });
        
    }
    if(currentTab === "arts"){
        var tgbutton = document.getElementById("tagbutton");
        document.getElementById("tg0").addEventListener('click', function(){
            tgbutton.innerText = "Tags";
            resetAll("tg", 8)
            document.getElementById("tg0").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Tags";
            filterByTag(currentSort);

        });
        document.getElementById("tg1").addEventListener('click', function(){
            tgbutton.innerText = "Art";
            resetAll("tg", 8)
            document.getElementById("tg1").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Art";
            filterByTag(currentSort);

        });
        document.getElementById("tg2").addEventListener('click', function(){
            tgbutton.innerText = "Lit";
            resetAll("tg", 8)
            document.getElementById("tg2").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Lit";
            filterByTag(currentSort);

        });
        document.getElementById("tg3").addEventListener('click', function(){
            tgbutton.innerText = "Pub. Speaking";
            resetAll("tg", 8)
            document.getElementById("tg3").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Pub. Speaking";
            filterByTag(currentSort);

        });
        document.getElementById("tg4").addEventListener('click', function(){
            tgbutton.innerText = "Lang/Culture";
            resetAll("tg", 8)
            document.getElementById("tg4").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Lang/Culture";
            filterByTag(currentSort);

        });
        document.getElementById("tg5").addEventListener('click', function(){
            tgbutton.innerText = "Drama";
            resetAll("tg", 8)
            document.getElementById("tg5").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Drama";
            filterByTag(currentSort);

        });
        document.getElementById("tg6").addEventListener('click', function(){
            tgbutton.innerText = "Music";
            resetAll("tg", 8)
            document.getElementById("tg6").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Music";
            filterByTag(currentSort);

        });
        document.getElementById("tg7").addEventListener('click', function(){
            tgbutton.innerText = "Film";
            resetAll("tg", 8)
            document.getElementById("tg7").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Film";
            filterByTag(currentSort);

        });
    }

    if(currentTab === "misc"){
        var tgbutton = document.getElementById("tagbutton");
        document.getElementById("tg0").addEventListener('click', function(){
            tgbutton.innerText = "Tags";
            resetAll("tg", 8)
            document.getElementById("tg0").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Tags";
            filterByTag(currentSort);

        });
        document.getElementById("tg1").addEventListener('click', function(){
            tgbutton.innerText = "Business";
            resetAll("tg", 8)
            document.getElementById("tg1").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Business";
            filterByTag(currentSort);

        });
        document.getElementById("tg2").addEventListener('click', function(){
            tgbutton.innerText = "Volunteering";
            resetAll("tg", 8)
            document.getElementById("tg2").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Volunteering";
            filterByTag(currentSort);

        });
        document.getElementById("tg3").addEventListener('click', function(){
            tgbutton.innerText = "Region";
            resetAll("tg", 8)
            document.getElementById("tg3").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Region";
            filterByTag(currentSort);

        });
        document.getElementById("tg4").addEventListener('click', function(){
            tgbutton.innerText = "Social Studies";
            resetAll("tg", 8)
            document.getElementById("tg4").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Social Studies";
            filterByTag(currentSort);

        });
        document.getElementById("tg5").addEventListener('click', function(){
            tgbutton.innerText = "Life Skills";
            resetAll("tg", 8)
            document.getElementById("tg5").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Life Skills";
            filterByTag(currentSort);

        });
        document.getElementById("tg6").addEventListener('click', function(){
            tgbutton.innerText = "Trade-Specific";
            resetAll("tg", 8)
            document.getElementById("tg6").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Trade-Specific";
            filterByTag(currentSort);

        });
        document.getElementById("tg7").addEventListener('click', function(){
            tgbutton.innerText = "Trivia";
            resetAll("tg", 8)
            document.getElementById("tg7").setAttribute("style", "background-color: #e0dcdc")
            currentSort[2] = "Trivia";
            filterByTag(currentSort);

        });
    }
    
});

var schoolButton = document.getElementById("school");
 
document.getElementById("s0").addEventListener('click', function(){

    if(schoolButton.innerText != "Troy High School"){

        schoolButton.innerText = "Troy High School";
        resetAll("s", 3)
        document.getElementById("s0").setAttribute("style", "background-color: #e0dcdc")
    
        cardData = []
        cardDtaFull.forEach(function(e) {
            
            if(e.school === "THS"){cardData.push(e)}
        })
        reloadDOM()
        
        loadDOM(cardData)

    }
    

});
document.getElementById("s1").addEventListener('click', function(){

    if(schoolButton.innerText != "Cranbrook"){

        schoolButton.innerText = "Cranbrook";
        resetAll("s", 3)
        document.getElementById("s1").setAttribute("style", "background-color: #e0dcdc")
        cardData = []
        cardDtaFull.forEach(function(e) {
            
            if(e.school === "C"){cardData.push(e)}
        })
        reloadDOM()
        loadDOM(cardData)
        submitRating();

    }
    

});
document.getElementById("s2").addEventListener('click', function(){

    if(schoolButton.innerText != "Rochester Adams"){

        schoolButton.innerText = "Rochester Adams";
        resetAll("s", 3)
        document.getElementById("s2").setAttribute("style", "background-color: #e0dcdc")
        cardData = []
        cardDtaFull.forEach(function(e) {
            
            if(e.school === "A"){cardData.push(e)}
        })
    
        reloadDOM()
        loadDOM(cardData)
        submitRating();
    }
    

});
   





var hrbutton = document.getElementById("hrbutton");
 
document.getElementById("hrs0").addEventListener('click', function(){
    hrbutton.innerText = "Hours";
    resetAll("hrs", 5)
    document.getElementById("hrs0").setAttribute("style", "background-color: #e0dcdc")
    currentSort[0] = "Hours";
    filterByTag(currentSort);

});
document.getElementById("hrs1").addEventListener('click', function(){
    hrbutton.innerText = "0";
    resetAll("hrs", 5)
    document.getElementById("hrs1").setAttribute("style", "background-color: #e0dcdc")
    currentSort[0] = "0";
    filterByTag(currentSort);

});
document.getElementById("hrs2").addEventListener('click', function(){
    hrbutton.innerText = "1-5";
    resetAll("hrs", 5)
    document.getElementById("hrs2").setAttribute("style", "background-color: #e0dcdc")
    currentSort[0] = "1-5";
    filterByTag(currentSort);

});
document.getElementById("hrs3").addEventListener('click', function(){
    hrbutton.innerText = "6-10";
    resetAll("hrs", 5)
    document.getElementById("hrs3").setAttribute("style", "background-color: #e0dcdc")
    currentSort[0] = "6-10";
    filterByTag(currentSort);

});
document.getElementById("hrs4").addEventListener('click', function(){
    hrbutton.innerText = "10+";
    resetAll("hrs", 5)
    document.getElementById("hrs4").setAttribute("style", "background-color: #e0dcdc")
    currentSort[0] = "10+";
    filterByTag(currentSort);

});





var cstbutton = document.getElementById("costbutton");
 
document.getElementById("cst0").addEventListener('click', function(){
    cstbutton.innerText = "Cost";
    // Changes color of tag to indicate selected
    resetAll("cst", 5)
    document.getElementById("cst0").setAttribute("style", "background-color: #e0dcdc")
    currentSort[1] = "Cost";
    filterByTag(currentSort);

    
});
document.getElementById("cst1").addEventListener('click', function(){
    cstbutton.innerText = "0";
    resetAll("cst", 5)
    document.getElementById("cst1").setAttribute("style", "background-color: #e0dcdc")
    currentSort[1] = "0";
    filterByTag(currentSort);

});
document.getElementById("cst2").addEventListener('click', function(){
    cstbutton.innerText = "1-20";
    resetAll("cst", 5)
    document.getElementById("cst2").setAttribute("style", "background-color: #e0dcdc")
    currentSort[1] = "1-20";
    filterByTag(currentSort);

});
document.getElementById("cst3").addEventListener('click', function(){
    cstbutton.innerText = "20-50";
    resetAll("cst", 5)
    document.getElementById("cst3").setAttribute("style", "background-color: #e0dcdc")
    currentSort[1] = "20-50";
    filterByTag(currentSort);

});
document.getElementById("cst4").addEventListener('click', function(){
    cstbutton.innerText = "50+";
    resetAll("cst", 5)
    document.getElementById("cst4").setAttribute("style", "background-color: #e0dcdc")
    currentSort[1] = "50+";
    filterByTag(currentSort);

});





//MODAL STUFF

// Get the modal
var modal = document.getElementById("myModal");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When user clicks on comment, submits

var post= document.getElementById("post");
post.addEventListener("click", function(){
    var commentBoxValue= document.getElementById("comment-box").value;
    if(localStorage.getItem(tit) != null){
        notif = document.getElementById("notif");
        notif.click();
        document.getElementById("comment-box").value = "";
    }
    else if(commentBoxValue === ""){

    }
    else{
            var name;
            if( localStorage.getItem("name") != null){
                name = localStorage.getItem("name");
            }
            else{
                name = "Anonymous";
            }

            // Post
            var li = document.createElement("li");
            var text = document.createTextNode("You: " + commentBoxValue);
            li.appendChild(text);
            li.setAttribute("class", "comment-val");
            document.getElementById("unordered").appendChild(li);

            info = findByTitle(tit);

            info["commentsUnapproved"].push(name + ": " + commentBoxValue)
            document.getElementById("comment-box").value = "";
            localStorage.setItem(tit, commentBoxValue);
            currentComments[tit] = "You: " + commentBoxValue;

            postData(databaseLink + '/savecourse/', info)
        
    }
    
    

});

// Accepts an enter as a submit

var input = document.getElementById("comment-box");
input.addEventListener("keyup", function(event) {
    if (event.key === "Enter") {
        event.preventDefault();
        post.click();
    }
});

// Close modal when click out

// Closes through X button

span.onclick = function() {
    modal.style.display = "none";
    var commeBox= document.getElementById("unordered");
    while (commeBox.firstChild) {
        commeBox.removeChild(commeBox.firstChild);
    }
    document.getElementById("comment-box").value = "";

    
}

// Closes by clicking out
window.onclick = function(event) {
    if (event.target === modal) {
        modal.style.display = "none";
        var commeBox= document.getElementById("unordered");
        while (commeBox.firstChild) {
            commeBox.removeChild(commeBox.firstChild);
        }
        document.getElementById("comment-box").value = "";

    }
}

function reloadDOM(){

    const leftS = document.getElementById("leftstem").innerHTML = "";
    const rightS = document.getElementById("rightstem").innerHTML = "";
    const leftSp = document.getElementById("leftsports").innerHTML = "";
    const rightSp = document.getElementById("rightsports").innerHTML = "";
    const leftA = document.getElementById("leftarts").innerHTML = "";
    const rightA = document.getElementById("rightarts").innerHTML = "";
    const leftM = document.getElementById("leftmisc").innerHTML = "";
    const rightM = document.getElementById("rightmisc").innerHTML = "";

    document.getElementById("tagbutton").innerText = "Tags";
    document.getElementById("hrbutton").innerText = "Hours";
    document.getElementById("costbutton").innerText = "Cost";
    resetAll("cst", 5);
    resetAll("hrs", 5);

    currentSort = ["Hours", "Cost", "Tags"];

    

}