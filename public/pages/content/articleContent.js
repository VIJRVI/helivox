window.onload = function() {

    let body = document.getElementById("content")

    let params = new URLSearchParams(location.search);
    let id = params.get("id");
    let firstPromise = fetch("https://helivox-361801.uc.r.appspot.com/findArticle/" + id);
    let secondPromise = firstPromise.then(response => response.json());
    secondPromise.then(cardDta => {
        document.getElementById("loader").remove()
        document.getElementById('coloration').setAttribute("style", "background-color: " + cardDta["color"])
        let title = document.createElement('h5');
        title.setAttribute('class', 'h1 card-title mb-0')
        title.innerText = cardDta['title'];
        body.appendChild(title);
        body.innerHTML += "<br><br>"
        
        let image = document.createElement("img");
        image.setAttribute("class", "card-img-top img-thumbnail col-lg-6 center-block d-block mx-auto");
        image.setAttribute("src", cardDta['titleImage']);
        body.appendChild(image);
        body.innerHTML += "<br>"
        let note = document.createElement('small');
        note.setAttribute('class', "text-muted");
        note.innerText = "by " + cardDta["author"] + " on " + cardDta["date"]
        body.appendChild(note)
        body.innerHTML += "<br><br><br>"
        body.innerHTML += cardDta['content']
    });


    if(window.innerWidth <= 990){
        let log_in = document.createElement("li")
        log_in.setAttribute("class", "nav-item")
        let log_inner = document.createElement("a")
        log_inner.setAttribute("href", "login.html")
        log_inner.setAttribute("class", "nav-link")
        let log_content = document.createElement("span")
        log_content.setAttribute("class", "nav-link-inner--text")
        if(localStorage.getItem("name") != null){log_content.innerText = "Logged in"}
        else {log_content.innerText = "Login"}
        
        log_inner.appendChild(log_content)
        log_in.appendChild(log_inner)
        document.getElementById("nav-location").appendChild(log_in)
    }
    if(localStorage.getItem("name") != null){
        document.getElementById("login-val").innerText = localStorage.getItem("name")
    }
    if(localStorage.getItem("admin") != null){
        document.getElementById("login-val").innerText = "Admin"
    }
};