var counter = 0;
var cardHold;

var databaseLink = "https://helivox-361801.uc.r.appspot.com"

let firstPromise = fetch(databaseLink + "/getarticles");
let secondPromise = firstPromise.then(response => response.json());
secondPromise.then(cardDta => {
    document.getElementById("loader").remove()
    
    cardDta.sort(compare);
    cardDta.reverse();
    cardHold = cardDta;
    for(let i = 0; i < cardDta.length; i++){
        newCard(cardDta[i], i)

    }
        

});


function compare( a, b )
  {
  if ( a['date'] < b['date']){
    return -1;
  }
  if ( a['date'] > b['date']){
    return 1;
  }
  return 0;
}
window.onload = function() {
  if(window.innerWidth <= 990){
    let log_in = document.createElement("li")
    log_in.setAttribute("class", "nav-item")
    let log_inner = document.createElement("a")
    log_inner.setAttribute("href", "login.html")
    log_inner.setAttribute("class", "nav-link")
    let log_content = document.createElement("span")
    log_content.setAttribute("class", "nav-link-inner--text")
    if(localStorage.getItem("name") != null){log_content.innerText = "Logged in"}
    else {log_content.innerText = "Login"}
    
    log_inner.appendChild(log_content)
    log_in.appendChild(log_inner)
    document.getElementById("nav-location").appendChild(log_in)
  }
  if(localStorage.getItem("name") != null){
      document.getElementById("login-val").innerText = localStorage.getItem("name")
  }
  if(localStorage.getItem("admin") != null){
      document.getElementById("login-val").innerText = "Admin"
  }
};

function newCard(cardDta, i){
    let ht = document.getElementById(counter%3)

    let main = document.createElement('div');
    main.setAttribute("class", "card ");

    let image = document.createElement("img");
    image.setAttribute("class", "card-img-top");
    image.setAttribute("src", cardDta['titleImage']);
    main.appendChild(image);

    let body = document.createElement('div');
    body.setAttribute('class', "card-body");
    

    let title = document.createElement('h5');
    title.setAttribute('class', "h2 card-title mb-0");
    title.innerText = cardDta["title"];
    body.appendChild(title)

    let note = document.createElement('small');
    note.setAttribute('class', "text-muted");
    note.innerText = "by " + cardDta["author"] + " on " + cardDta["date"]

    body.appendChild(note);

    body.innerHTML += "<br>"

    let link = document.createElement("a");
    link.setAttribute("class", "btn btn-link px-0");
    link.setAttribute("href", "articleContent.html" + "?id=" + cardDta["id"]);
    link.innerText = "View article";

    body.appendChild(link);




    main.appendChild(body);


    ht.appendChild(main);

    counter++;

}



async function postData(url = '', data = {}) {
    // Default options are marked with *
    const response = await fetch(url, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        headers: {
        'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: JSON.stringify(data) // body data type must match "Content-Type" header
    });
    
        return response; // parses JSON response into native JavaScript objects
}









    

