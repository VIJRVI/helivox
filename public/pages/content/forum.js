
var databaseLink = "https://helivox-361801.uc.r.appspot.com"
let numLikeClicks = {};
let numObjects = 0;
let allData;
let currentSort = ["Most Recent", "none"]
let isBlue = []
if(localStorage.getItem("admin") != null){
    showElem();
}

//makes most recent appear grayed 
document.getElementById("soon").setAttribute("style", "background-color: #e0dcdc")

let firstPromise = fetch(databaseLink + "/getforum");
let secondPromise = firstPromise.then(response => response.json());
secondPromise.then(cardData => {
    if(cardData["status"] != "200"){
        if(cardData["status"] != undefined){
            alert("Error: " + cardData["status"])
        }
        

    }
    document.getElementById("loader").remove()
    allData = cardData
    //Sorts by time on init
    cardData = sortTime(cardData);
    for(let i = 0; i < cardData.length; i++){
        loadDOM(cardData[i])
        numLikeClicks[cardData[i]["id"]] = 0;
    }
    


});























function loadDOM(alrt){
   
    // Change vals based on class
    upperClass = ""
    icon = ""
    alrt["content"] = alrt["content"].replace("<img", "<img class = 'testimg' ")

    
    if(alrt["type"] == "stem"){
        icon = "ni ni-chart-bar-32"
        upperClass = "alert alert-success alrt"
    }
    else if(alrt["type"] == "sports"){
        icon = "ni ni-trophy"
        upperClass = "alert alert-info alrt"
    }
    else if(alrt["type"] == "arts"){
        icon = "ni ni-image"
        upperClass = "alert alert-primary alrt"
    }
    else if(alrt["type"] == "misc"){
        icon = "ni ni-planet"
        upperClass = "alert alert-default alrt"
    }


    //Build actual html

    let upperLvl = document.createElement("div");
    upperLvl.className = upperClass;
    upperLvl.setAttribute("id", alrt["id"])
    upperLvl.setAttribute("role", "alert");

    //Icon

    let iconElem = document.createElement("span");
    iconElem.setAttribute("class", "alert-icon");
    let iconInner = document.createElement("i");
    iconInner.setAttribute("class", icon);
    iconElem.appendChild(iconInner);
    upperLvl.appendChild(iconElem);

    //Date

    let date = document.createElement("span");
    date.setAttribute("style", "float: right;")
    //reformats date
	let t = alrt["date"].split("-");
	let tempDate = t[1] + "/" + t[2] + "/" + t[0]
    date.innerText = tempDate;
    upperLvl.appendChild(date);

    //Title

    let titleElem = document.createElement("span");
    titleElem.setAttribute("class", "alert-text");
    let titleInner = document.createElement("strong");
    titleInner.innerText = alrt["title"];
    titleElem.appendChild(titleInner);
    upperLvl.appendChild(titleElem);

    //Main content

    let main = document.createElement("div");
    main.setAttribute("class", "cont");
    main.innerHTML += alrt["content"];

    let checkboxHolder = document.createElement("div");
    checkboxHolder.setAttribute("style", "margin-top:10px; -moz-user-select: none; -webkit-user-select: none; -ms-user-select:none; user-select:none;-o-user-select:none;")
    checkboxHolder.setAttribute("id", numObjects);

    let checkbox = document.createElement("i");
    
    checkbox.innerHTML = '<i onclick = "changeLikeNum(' + numObjects + ')"' +' class="ni ni-check-bold" style = "cursor: pointer;"></i>'+ alrt["likes"];
    if(isBlue.includes(alrt["id"])){
        checkbox.innerHTML =  '<i onclick = "changeLikeNum(' + numObjects + ')"' +' class="ni ni-check-bold" style = "cursor: pointer; color:blue;"></i>'+ alrt["likes"];
    }
    checkboxHolder.appendChild(checkbox);

    main.appendChild(checkboxHolder);

    upperLvl.appendChild(main);

    paste = document.getElementById("holderElem");

    paste.appendChild(upperLvl)
    
    numObjects++;
}


              



function showElem(){
    let add = document.getElementById("addElem");
    add.style.display = "block";
}

function changeLikeNum(elemId){

    // Complicated because when sorting, need to remember which elements have been liked > made dictionary of IDs from object
    if(numLikeClicks[allData[elemId]["id"]] % 2 == 0){
        allData[elemId]["likes"] += 1
        document.getElementById(elemId).innerHTML =  '<i onclick = "changeLikeNum(' + elemId + ')"' +' class="ni ni-check-bold" style = "cursor: pointer; color:blue;"></i>'+ allData[elemId]["likes"];
        isBlue.push(allData[elemId]["id"])
    }
    else{
        allData[elemId]["likes"] -= 1
        document.getElementById(elemId).innerHTML =  '<i onclick = "changeLikeNum(' + elemId + ')"' +' class="ni ni-check-bold" style = "cursor: pointer;"></i>'+ allData[elemId]["likes"]
        const index = isBlue.indexOf(allData[elemId]["id"]);
        if (index > -1) {
            isBlue.splice(index, 1); // 2nd parameter means remove one item only
        }

    }
    numLikeClicks[allData[elemId]["id"]]++;

    

    
    postData(databaseLink + '/saveforum/', allData[elemId]).then(data => {
        if(data["status"] != "200"){
            alert("Error: " + data["status"])
        }

    })
    
    
}

async function postData(url = '', data = {}) {
    // Default options are marked with *
    const response = await fetch(url, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        headers: {
        'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: JSON.stringify(data) // body data type must match "Content-Type" header
    });
    
        return response; // parses JSON response into native JavaScript alrtects
}

function resetAll(name, num){
    for(let i = 0; i  < num; i++){
        document.getElementById(name+ i).setAttribute("style", "background-color: #fffcfc")
    }
}

function removeAllChildNodes(parent) {
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}
window.onload = function() {
    if(window.innerWidth <= 990){
        let log_in = document.createElement("li")
        log_in.setAttribute("class", "nav-item")
        let log_inner = document.createElement("a")
        log_inner.setAttribute("href", "login.html")
        log_inner.setAttribute("class", "nav-link")
        let log_content = document.createElement("span")
        log_content.setAttribute("class", "nav-link-inner--text")
        if(localStorage.getItem("name") != null){log_content.innerText = "Logged in"}
        else {log_content.innerText = "Login"}
        
        log_inner.appendChild(log_content)
        log_in.appendChild(log_inner)
        document.getElementById("nav-location").appendChild(log_in)
    }
    if(localStorage.getItem("name") != null){
        document.getElementById("login-val").innerText = localStorage.getItem("name")
    }
    if(localStorage.getItem("admin") != null){
        document.getElementById("login-val").innerText = "Admin"
    }
};

function filterByTag(sortable){
    if(sortable[0] === "soon"){

            numObjects = 0;
            removeAllChildNodes(document.getElementById("holderElem"))

            allData = sortTime(allData)
            for(let i = 0; i < allData.length; i++){
                loadDOM(allData[i])
            }
        }
        else{
            numObjects = 0;
            removeAllChildNodes(document.getElementById("holderElem"))
            let orig = allData
            allData.sort((a, b) => (a["likes"] > b["likes"]) ? -1 : 1)
            for(let i = 0; i < allData.length; i++){
                loadDOM(allData[i])
            }
            allData = orig
        }
  
    for(let r = 0; r < allData.length; r++){
        document.getElementById(allData[r]["id"]).style.display = "block";
    
        // Tags

        if(sortable[1] != "none"){
           if(!(allData[r]["type"] === (sortable[1]))){
                document.getElementById(allData[r]["id"]).style.display = "none";
           }
        }
    }


}

function sortTime(dta){
    let ans = []
    let temp = {}

    const d = new Date();
    let currDate = d.getFullYear() + "-" + String(d.getMonth()+1).padStart(2, "0") + "-" + String(d.getDate()).padStart(2, "0");

    // Remove old dates

    for(let t = 0; t < dta.length; t++){
        if(currDate > dta[t]["date"]){
             // Remove from backend
            postData(databaseLink + '/deleteforum/', dta[t])

            // Remove for user
            dta.splice(t, 1)
           
        }
    }
    
    
    for(let t = 0; t < dta.length; t++){
        let curDate = "";
        if(Object.keys(temp).includes(dta[t]["date"])){
            curDate = dta[t]["date"] + "x" + t
            
        }
        else{
            curDate = dta[t]["date"]
        }
        temp[curDate] = t
    }
    let sorted = Object.keys(temp).sort()
    for (let e = 0; e < sorted.length; e++){
        ans.push(dta[temp[sorted[e]]])
    }

    return ans
    
}


// Event listeners for sort

var orderbutton = document.getElementById("order");
 
document.getElementById("soon").addEventListener('click', function(){
    orderbutton.innerText = "Most Recent";
    document.getElementById("soon").setAttribute("style", "background-color: #e0dcdc")
    document.getElementById("like").setAttribute("style", "background-color: #fffcfc")
    
    currentSort[0] = "soon";
    filterByTag(currentSort);

});

document.getElementById("like").addEventListener('click', function(){
    orderbutton.innerText = "Most Liked";
    document.getElementById("like").setAttribute("style", "background-color: #e0dcdc")
    document.getElementById("soon").setAttribute("style", "background-color: #fffcfc")
    
    currentSort[0] = "liked";
    filterByTag(currentSort);

});





var topicbutton = document.getElementById("topic");
 
document.getElementById("t0").addEventListener('click', function(){
    topicbutton.innerText = "Topic";
    resetAll("t", 5)   
    document.getElementById("t0").setAttribute("style", "background-color: #e0dcdc")
     
    currentSort[1] = "none";
    filterByTag(currentSort);

});

document.getElementById("t1").addEventListener('click', function(){
    topicbutton.innerText = "Stem";
    resetAll("t", 5)
    document.getElementById("t1").setAttribute("style", "background-color: #e0dcdc")
    
    currentSort[1] = "stem";
    filterByTag(currentSort);

});
document.getElementById("t2").addEventListener('click', function(){
    topicbutton.innerText = "Sports";
    resetAll("t", 5)   
    document.getElementById("t2").setAttribute("style", "background-color: #e0dcdc")
     
    currentSort[1] = "sports";
    filterByTag(currentSort);

});

document.getElementById("t3").addEventListener('click', function(){
    topicbutton.innerText = "Art";
    resetAll("t", 5)   
    document.getElementById("t3").setAttribute("style", "background-color: #e0dcdc")
     
    currentSort[1] = "arts";
    filterByTag(currentSort);

});

document.getElementById("t4").addEventListener('click', function(){
    topicbutton.innerText = "Misc";
    resetAll("t", 5)   
    document.getElementById("t4").setAttribute("style", "background-color: #e0dcdc")
     
    currentSort[1] = "misc";
    filterByTag(currentSort);

});


