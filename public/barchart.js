

var databaseLink = "https://helivox-361801.uc.r.appspot.com"

let url = databaseLink + "/getall";
let firstPromise = fetch(url);
let secondPromise = firstPromise.then(response => response.json());
secondPromise.then(data => {
    document.getElementById("loader").remove()
    var width = window.innerWidth;
    
   

    const shopImg = document.getElementById("shopImg");
    if(width <= 700){
        shopImg.setAttribute("style", "margin-top: 40px; width: 25rem");
    }

    const gifS = document.getElementById("gifsection");
    const gifD = document.getElementById("gifdiv");
    if(width <= 1500){
        gifS.setAttribute("style", "background: url(cube.gif) no-repeat center; background-size: 100%; object-fit: cover;");
        gifD.setAttribute("style", "");
    }

});

// window.onload = function() {
//     const shopImg = document.getElementById("shopImg");
//     if(width <= 700){
//         shopImg.setAttribute("style", "margin-top: 40px; width: 400px");
//     }
// }



$(document).ready(function (){
    $("#click").click(function (){
        $('html, body').animate({
            scrollTop: $("#graph").offset().top
        }, 2000);
    });
});

$(document).ready(function (){
    $("#click1").click(function (){
        $('html, body').animate({
            scrollTop: $("#opportunities").offset().top
        }, 2000);
    });
});

function newsletter(){
    console.log("nothing yet")
}


function newCard(cardDta, i){
    let ht = document.getElementById(i)
    let main = document.createElement('div');
    main.setAttribute("class", "card ");

    let image = document.createElement("img");
    image.setAttribute("class", "card-img-top");
    image.setAttribute("src", cardDta['titleImage']);
    main.appendChild(image);

    let body = document.createElement('div');
    body.setAttribute('class', "card-body");

    let title = document.createElement('h5');
    title.setAttribute('class', "h2 card-title mb-0");
    title.innerText = cardDta["title"];
    body.appendChild(title)

    let note = document.createElement('small');
    note.setAttribute('class', "text-muted");
    note.innerText = "by " + cardDta["author"] + " on " + cardDta["date"]

    body.appendChild(note);

    body.innerHTML += "<br>"

    let link = document.createElement("a");
    link.setAttribute("class", "btn btn-link px-0");
    link.setAttribute("href", "pages/content/articles.html");
    link.innerText = "View article";

    body.appendChild(link);




    main.appendChild(body);


    ht.appendChild(main);


}

let firstPromiseArticles = fetch(databaseLink + "/getarticles");
let secondPromiseArticles = firstPromiseArticles.then(response => response.json());
secondPromiseArticles.then(cardDta => {
    cardDta.sort(compare);
    cardDta.reverse();
    for(let i = 0; i < 2; i++){
        newCard(cardDta[i], i)
    }
});

function compare( a, b )
  {
  if ( a['date'] < b['date']){
    return -1;
  }
  if ( a['date'] > b['date']){
    return 1;
  }
  return 0;
}


function addCarousel(cardDta, i){
    let firstPos = document.getElementById("carousel1");
    let secondPos = document.getElementById("carousel2");

    let out = document.createElement("li");
    if(i === 0){
        out.setAttribute("class", "active")
    }
    out.setAttribute("data-target", "#demo");
    out.setAttribute("data-slide-to", i.toString())

    firstPos.appendChild(out)

    let inn1 = document.createElement("div");
    if(i === 0){
        inn1.setAttribute("class", "carousel-item active")
    }
    else{
        inn1.setAttribute("class", "carousel-item")
    }
    inn1.setAttribute("data-interval", "4000")

    let carLink = document.createElement("a")
    if(cardDta["opImg"] === null){
        carLink.setAttribute("href", cardDta["originURL"])
    }
    else{
        carLink.setAttribute("href", "pages/content/landing.html" + "?id=" + cardDta["id"])
    }
    

    let carImg = document.createElement("img")
    carImg.setAttribute("src", cardDta["img"])
    carImg.setAttribute("class", "img-fluid")

    carLink.appendChild(carImg)
    inn1.appendChild(carLink)
    secondPos.appendChild(inn1)



}


let firstPromiseCar = fetch(databaseLink + "/getCarousel");
let secondPromiseCar = firstPromiseCar.then(response => response.json());
secondPromiseCar.then(cardDta => {
    for(let i = 0; i < cardDta.length; i++){
        addCarousel(cardDta[i], i)
    }
});



window.onload = function() {
    if(window.innerWidth <= 990){
        document.getElementById("articleSection").setAttribute("style", "")
        let log_in = document.createElement("li")
        log_in.setAttribute("class", "nav-item")
        let log_inner = document.createElement("a")
        log_inner.setAttribute("href", "pages/content/login.html")
        log_inner.setAttribute("class", "nav-link")
        let log_content = document.createElement("span")
        log_content.setAttribute("class", "nav-link-inner--text")
        if(localStorage.getItem("name") != null){log_content.innerText = "Logged in"}
        else {log_content.innerText = "Login"}
        
        log_inner.appendChild(log_content)
        log_in.appendChild(log_inner)
        document.getElementById("nav-location").appendChild(log_in)
    }
    if(localStorage.getItem("name") != null){
        document.getElementById("login-val").innerText = localStorage.getItem("name")
    }
    if(localStorage.getItem("admin") != null){
        document.getElementById("login-val").innerText = "Admin"
    }

    
};
