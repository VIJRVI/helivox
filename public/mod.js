var imgLink;
var imgLinkCH;
var selectedComponents = []
var cardDataStore;
var allComponents = ["Title", "Image", "Category", "Description", "Link", "Tags", "Hours Commitment", "Cost"]
var objVals = ["title", "image", "category", "description", "link", "tags", "hrsCommit", "cost"]


var typeOfClass;


var databaseLink = "https://helivox-361801.uc.r.appspot.com"


var toBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
});


function hours(){
    document.getElementById("dtaIN").disabled = true;
    document.getElementById("dtaIN").setAttribute("style", "background-color:#696969");
    fetch(databaseLink + "/savedta?hours=" + document.getElementById("data").value.trim()).then(data => {
        if(data["status"] != "200"){
            alert("Error: " + data["status"])
        }
        document.getElementById("dtaIN").disabled = false;
        document.getElementById("data").value = "";
        document.getElementById("dtaIN").setAttribute("style", "");
    })

}

// Article Cycling:
var articleHold;

function returnArticle(){
    let artName = document.getElementById("articleFindName").value;
    document.getElementById("returnBtn").disabled = true;
    document.getElementById("returnBtn").setAttribute("style", "background-color:#696969");;

    let firstPromiseArt = fetch(databaseLink + "/find-article/" + artName.trim());
    let secondPromiseArt = firstPromiseArt.then(response => response.json());
    secondPromiseArt.then(cardDta => {
        if(cardDta[0]["status"] != undefined){
            alert("Error: " + data["status"])
        }
        articleHold = cardDta;
        document.getElementById("returnBtn").disabled = false;
        document.getElementById("returnBtn").setAttribute("style", "");;

        if(cardDta.length === 0){
            alert("Nothing Available")
        }
        else{
            displayArticle(cardDta[0])

            document.getElementById('articleFinderForm').setAttribute('style', 'display:none')
            document.getElementById('articleButtons').setAttribute('style', 'display:block')
        }
        
    });
}
function findNextArticle(){
    articleHold.shift();
    if(articleHold.length != 0){
        document.getElementById("articleContentHolder").innerHTML = ""
        displayArticle(articleHold[0])
    }
    else{
        alert("Nothing Available")
        document.getElementById('articleFinderForm').setAttribute('style', 'display:block')
        document.getElementById('articleButtons').setAttribute('style', 'display:none')
        document.getElementById("articleContentHolder").innerHTML = ""
        document.getElementById("articleFindName").value = "";

    }
}

function deleteArticle(){

    postData(databaseLink + '/deletearticle', articleHold[0])
    .then(data => {
        document.getElementById('articleFinderForm').setAttribute('style', 'display:block')
        document.getElementById('articleButtons').setAttribute('style', 'display:none')
        document.getElementById("articleContentHolder").innerHTML = ""
        document.getElementById("articleFindName").value = "";
    });


  

}

function displayArticle(cardDta){
        let body = document.getElementById("articleContentHolder")

        let image = document.createElement("img");
        image.setAttribute("class", "card-img-top");
        image.setAttribute("style", "width:100%")
        image.setAttribute("src", cardDta['titleImage']);
        body.appendChild(image);
        body.innerHTML += "<br>"
        let note = document.createElement('small');
        note.setAttribute('class', "text-muted");
        note.innerText = "by " + cardDta["author"] + " on " + cardDta["date"]
        body.appendChild(note)
        body.innerHTML += "<br><br><br>"
        body.innerHTML += cardDta['content']
}










// If user changes # of tags:

var tgC = document.getElementById('tgs');

tgC.addEventListener("change", changeCategory);



function changeCategory(){
    divInp = document.getElementById("location");

    while (divInp.firstChild) {
        divInp.removeChild(divInp.firstChild);
    }

    for(let y = 0; y < parseInt(tgC.value.trim())-1; y++){
                
        var divHold = document.createElement("div");
        divHold.setAttribute("class", "tagHold");
        var newSelect = document.createElement("select");
        newSelect.setAttribute("id", "tag" + (y+1));
        newSelect.setAttribute("class", "form-control fakeBr" );
        divHold.appendChild(newSelect);
        divInp.appendChild(divHold);

    }
    changeTag();
    
}


var sel = document.getElementById('cat');
        
sel.addEventListener("change", changeTag);

function changeTag() {
    var tag = [document.getElementById('tag0'), document.getElementById('tag1'), document.getElementById('tag2')];
    for(let u = 0; u < parseInt(tgC.value.trim()); u++){
        //delete current tags
        while (tag[u].options.length > 0) {
            tag[u].remove(0);
        }
        if ( sel.value.trim() === 'stem' ) {

            let science = new Option('Science','Science');
            tag[u].add(science,undefined);

            let math = new Option('Math','Math');
            tag[u].add(math,undefined);

            let med = new Option('Med','Med');
            tag[u].add(med,undefined);

            let cs = new Option('CS','CS');
            tag[u].add(cs,undefined);

            let tech = new Option('Technology','Technology');
            tag[u].add(tech,undefined);

            let sat = new Option('Standardized Testing','Standardized Testing');
            tag[u].add(sat,undefined);

            
        }
        if ( sel.value.trim() === 'sports' ) {

            let boys = new Option('Boys','Boys');
            tag[u].add(boys,undefined);

            let girls = new Option('Girls','Girls');
            tag[u].add(girls,undefined);

            let winter = new Option('Winter','Winter');
            tag[u].add(winter,undefined);

            let spring = new Option('Spring','Spring');
            tag[u].add(spring,undefined);

            let fall = new Option('Fall','Fall');
            tag[u].add(fall,undefined);

            let dance = new Option('Dance','Dance');
            tag[u].add(dance,undefined);

            let swim = new Option('Swim','Swim');
            tag[u].add(swim,undefined);

            let pfit = new Option('Personal Fitness','Personal Fitness');
            tag[u].add(pfit,undefined);

            let selfd = new Option('Self Defense','Self Defense');
            tag[u].add(selfd,undefined);


        }
        if ( sel.value.trim() === 'arts' ) {

            let art = new Option('Art','Art');
            tag[u].add(art,undefined);

            let lit = new Option('Lit','Lit');
            tag[u].add(lit,undefined);

            let pub = new Option('Pub. Speaking','Pub. Speaking');
            tag[u].add(pub,undefined);

            let lang = new Option('Lang/Culture','Lang/Culture');
            tag[u].add(lang,undefined);

            let dra = new Option('Drama','Drama');
            tag[u].add(dra,undefined);

            let sc = new Option('Music','Music');
            tag[u].add(sc,undefined);

            let grs = new Option('Film','Film');
            tag[u].add(grs,undefined);


        }
        if ( sel.value.trim() === 'misc' ) {
            let sdf = new Option('Business','Business');
            tag[u].add(sdf,undefined);

            let asfd = new Option('Volunteering','Volunteering');
            tag[u].add(asfd,undefined);
            
            let rf = new Option('Religion','Religion');
            tag[u].add(rf,undefined);

            let drd = new Option('Social Studies','Social Studies');
            tag[u].add(drd,undefined);

            let af = new Option('Life Skills','Life Skills');
            tag[u].add(af,undefined);

            let eb = new Option('Trade-Specific','Trade-Specific');
            tag[u].add(eb,undefined);

            let wef = new Option('Trivia','Trivia');
            tag[u].add(wef,undefined);


        
        }

    }

}








var tgCH = document.getElementById('tgsChange');

tgCH.addEventListener("change", changeCategoryCH);



function changeCategoryCH(){
    let divInp = document.getElementById("locationCH");

    while (divInp.firstChild) {
        divInp.removeChild(divInp.firstChild);
    }

    for(let y = 0; y < parseInt(tgCH.value.trim())-1; y++){
                
        var divHold = document.createElement("div");
        divHold.setAttribute("class", "tagHold");
        var newSelect = document.createElement("select");
        newSelect.setAttribute("id", "tg" + (y+1));
        newSelect.setAttribute("class", "form-control fakeBr" );
        divHold.appendChild(newSelect);
        divInp.appendChild(divHold);

    }
    changeTagCH();
    
}


var selCH = document.getElementById('CategoryCH');
        
selCH.addEventListener("change", changeTagCH);

function changeTagCH() {
    var tag = [document.getElementById('tg0'), document.getElementById('tg1'), document.getElementById('tg2')];
    for(let u = 0; u < parseInt(tgCH.value.trim()); u++){
        //delete current tags
        while (tag[u].options.length > 0) {
            tag[u].remove(0);
        }
        if ( selCH.value.trim() === 'stem' ) {

            let science = new Option('Science','Science');
            tag[u].add(science,undefined);

            let math = new Option('Math','Math');
            tag[u].add(math,undefined);

            let med = new Option('Med','Med');
            tag[u].add(med,undefined);

            let cs = new Option('CS','CS');
            tag[u].add(cs,undefined);

            let tech = new Option('Technology','Technology');
            tag[u].add(tech,undefined);

            let sat = new Option('Standardized Testing','Standardized Testing');
            tag[u].add(sat,undefined);

            
        }
        if ( selCH.value.trim() === 'sports' ) {

            let boys = new Option('Boys','Boys');
            tag[u].add(boys,undefined);

            let girls = new Option('Girls','Girls');
            tag[u].add(girls,undefined);

            let winter = new Option('Winter','Winter');
            tag[u].add(winter,undefined);

            let spring = new Option('Spring','Spring');
            tag[u].add(spring,undefined);

            let fall = new Option('Fall','Fall');
            tag[u].add(fall,undefined);

            let dance = new Option('Dance','Dance');
            tag[u].add(dance,undefined);

            let swim = new Option('Swim','Swim');
            tag[u].add(swim,undefined);

            let pfit = new Option('Personal Fitness','Personal Fitness');
            tag[u].add(pfit,undefined);

            let selfd = new Option('Self Defense','Self Defense');
            tag[u].add(selfd,undefined);


        }
        if ( selCH.value.trim() === 'arts' ) {

            let art = new Option('Art','Art');
            tag[u].add(art,undefined);

            let lit = new Option('Lit','Lit');
            tag[u].add(lit,undefined);

            let pub = new Option('Pub. Speaking','Pub. Speaking');
            tag[u].add(pub,undefined);

            let lang = new Option('Lang/Culture','Lang/Culture');
            tag[u].add(lang,undefined);

            let dra = new Option('Drama','Drama');
            tag[u].add(dra,undefined);

            let sc = new Option('Music','Music');
            tag[u].add(sc,undefined);

            let grs = new Option('Film','Film');
            tag[u].add(grs,undefined);


        }
        if ( selCH.value.trim() === 'misc' ) {
            let sdf = new Option('Business','Business');
            tag[u].add(sdf,undefined);

            let asfd = new Option('Volunteering','Volunteering');
            tag[u].add(asfd,undefined);
            
            let rf = new Option('Religion','Religion');
            tag[u].add(rf,undefined);

            let drd = new Option('Social Studies','Social Studies');
            tag[u].add(drd,undefined);

            let af = new Option('Life Skills','Life Skills');
            tag[u].add(af,undefined);

            let eb = new Option('Trade-Specific','Trade-Specific');
            tag[u].add(eb,undefined);

            let wef = new Option('Trivia','Trivia');
            tag[u].add(wef,undefined);


        
        }

    }

}

var isExtracurricular = false;


var assocSelect = document.getElementById('assoc');
assocSelect.onchange = (event) => {
     var inputText = event.target.value;
     if(inputText == "extracurricular"){
        document.getElementById("schoolInput").setAttribute("style", "display: none");
        document.getElementById("cityInput").setAttribute("style", "display: block");
        isExtracurricular = true;
     }
     else{
        document.getElementById("schoolInput").setAttribute("style", "display: block");
        document.getElementById("cityInput").setAttribute("style", "display: none");
        isExtracurricular = false;
     }
 }


function catalog(){


    var dta;
    var categories;
    var content;
    var title;
    var link;
    var tags = [];

    document.getElementById("ctlg").disabled = true;
    document.getElementById("ctlg").setAttribute("style", "background-color:#696969");


    categories = document.getElementById("cat").value.trim();




    //img = document.getElementById("img").value.trim();


    



    content = document.getElementById("cont").value.trim();
    title = document.getElementById("titl").value.trim();
    link = document.getElementById("link").value.trim();


    for(let u = 0; u < parseInt(tgC.value.trim()); u++){
        tags.push(document.getElementById("tag" + u).value.trim())
    }
    hrs = document.getElementById("hrs").value.trim();
    cost = document.getElementById("cost").value.trim();

    school = document.getElementById("school").value.trim();
    //parse data to backend
    city = document.getElementById("city").value.trim();

  

    cardDta = {
        "title": title.trim(),
        "category": categories.trim(),
        "description": content.trim(),
        "image": document.getElementById("img").value.trim(),
        "tags": tags,
        "link": link.trim(),
        "hrsCommit": hrs,
        "cost": cost,
        "school": school
    };

    if(isExtracurricular){
         cardDta = {
            "title": title.trim(),
            "category": categories.trim(),
            "description": content.trim(),
            "image": document.getElementById("img").value.trim(),
            "tags": tags,
            "link": link.trim(),
            "hrsCommit": hrs,
            "cost": cost,
            "city": city
        };
    }



    var assoc = document.getElementById("assoc").value.trim();
    if(assoc === "club"){
        postData(databaseLink + '/saveclub', cardDta)
        .then(data => {
        if(data["status"] != "200"){
            alert("Error: " + data["status"])
        }
        document.getElementById("img").value = "";
        document.getElementById("cont").value = "";
        document.getElementById("titl").value = "";
        document.getElementById("link").value = "";
        document.getElementById("hrs").value = "";
        document.getElementById("cost").value = "";
        document.getElementById("cat").selectedIndex = 0;
        document.getElementById("assoc").selectedIndex = 0;
        document.getElementById("tgs").selectedIndex = 0;
        document.getElementById("school").selectedIndex = 0;

        changeCategory()

        document.getElementById("ctlg").disabled = false;
        document.getElementById("ctlg").setAttribute("style", "");
    
        


            });
    }
    else if(assoc == "extracurricular"){

        postData(databaseLink + '/saveextracurriculars', cardDta)
        .then(data => {
        if(data["status"] != "200"){
            alert("Error: " + data["status"])
        }


        document.getElementById("img").value = "";
        document.getElementById("cont").value = "";
        document.getElementById("titl").value = "";
        document.getElementById("link").value = "";
        document.getElementById("hrs").value = "";
        document.getElementById("cost").value = "";
        document.getElementById("cat").selectedIndex = 0;
        document.getElementById("assoc").selectedIndex = 0;
        document.getElementById("tgs").selectedIndex = 0;
        document.getElementById("city").selectedIndex = 0;

        changeCategory()

        document.getElementById("ctlg").disabled = false;
        document.getElementById("ctlg").setAttribute("style", "");

        document.getElementById("schoolInput").setAttribute("style", "display: block");
        document.getElementById("cityInput").setAttribute("style", "display: none");
        isExtracurricular = false;
    
        


            });

    }
    else{
        postData(databaseLink + '/savecourse', cardDta)
        .then(data => {
            if(data["status"] != "200"){
                alert("Error: " + data["status"])
            }
            document.getElementById("img").value = "";
            document.getElementById("cont").value = "";
            document.getElementById("titl").value = "";
            document.getElementById("link").value = "";
            document.getElementById("hrs").value = "";
            document.getElementById("cost").value = "";;
            document.getElementById("cat").selectedIndex = 0;
            document.getElementById("assoc").selectedIndex = 0;
            document.getElementById("tgs").selectedIndex = 0;
            document.getElementById("school").selectedIndex = 0;
            changeCategory()    

            document.getElementById("ctlg").disabled = false;
            document.getElementById("ctlg").setAttribute("style", "");

        });
    }















}

function findCatalog(){
    document.getElementById("findBtn").disabled = true;
    document.getElementById("findBtn").setAttribute("style", "background-color:#696969");

    if(document.getElementById("typeOfClass").value.trim() === "course"){
        let firstPromiseStars = fetch(databaseLink + "/find-course-title/" + document.getElementById("titleOfClass").value.trim());
        let secondPromiseStars = firstPromiseStars.then(response => response.json());
        secondPromiseStars.then(cardDta => {
            if(cardDta.length != 0){
                if(cardDta[0]["status"] != "200" && cardDta[0]["status"] != undefined){
                    alert("Error: " + cardDta[0]["status"])
                }
                document.getElementById('CategoryCH').value = cardDta[0]["category"];
                changeTagCH();


                typeOfClass = "course"
                displayFoundData(cardDta)
            }
            document.getElementById("findBtn").disabled = false;
            document.getElementById("findBtn").setAttribute("style", "");
            document.getElementById("titleOfClass").value = ""
            document.getElementById("typeOfClass").selectedIndex = 0;
        });
    }

    else if(document.getElementById("typeOfClass").value.trim() === "extracurricular"){
        let firstPromiseStars = fetch(databaseLink + "/find-extracurricular-title/" + document.getElementById("titleOfClass").value.trim());
        let secondPromiseStars = firstPromiseStars.then(response => response.json());
        secondPromiseStars.then(cardDta => {

            if(cardDta.length != 0){
                if(cardDta[0]["status"] != "200" && cardDta[0]["status"] != undefined){
                    alert("Error: " + cardDta[0]["status"])
                }
                document.getElementById('CategoryCH').value = cardDta[0]["category"];
                changeTagCH();
    
                typeOfClass = "extracurricular"
                displayFoundData(cardDta)
            }
            document.getElementById("findBtn").disabled = false;
            document.getElementById("findBtn").setAttribute("style", "");
            document.getElementById("titleOfClass").value = ""
            document.getElementById("typeOfClass").selectedIndex = 0;
            

        });
    }
    else{
        let firstPromiseStars = fetch(databaseLink + "/find-title/" + document.getElementById("titleOfClass").value.trim());
        let secondPromiseStars = firstPromiseStars.then(response => response.json());
        secondPromiseStars.then(cardDta => {

            if(cardDta.length != 0){
                if(cardDta[0]["status"] != "200" && cardDta[0]["status"] != undefined){
                    alert("Error: " + cardDta[0]["status"])
                }

                document.getElementById('CategoryCH').value = cardDta[0]["category"];
                changeTagCH();


                typeOfClass = "club"
                displayFoundData(cardDta)
            }
            document.getElementById("findBtn").disabled = false;
            document.getElementById("findBtn").setAttribute("style", "");
            document.getElementById("titleOfClass").value = "";
            document.getElementById("typeOfClass").selectedIndex = 0;
            
        });
        
    }

    
}


function displayFoundData(cardDta){
    cardDataStore = cardDta;
    document.getElementById("finderForm").style.display = "none";

    if(cardDta.length === 0){
        document.getElementById("displayDta").innerText= "No Item Found" 
        document.getElementById("displayDta").innerHTML +="<br><br>"

        let returnButton = document.createElement("input")
        returnButton.setAttribute("type", "submit")
        returnButton.setAttribute("class", "btn")
        returnButton.setAttribute("value", "Return")
        returnButton.setAttribute("onclick", "returnBack()")
        document.getElementById("displayDta").appendChild(returnButton)
    }
    else{
        let dispImg = document.createElement("img")

        dispImg.setAttribute("src", cardDta[0]["image"])
        dispImg.setAttribute("class", "imgstd")
        dispImg.setAttribute("alt", "Extracurriculars")
    
        document.getElementById("displayDta").appendChild(dispImg)
        document.getElementById("displayDta").innerHTML += "<br><br>"
    
        let dispContent = document.createElement("p")
        dispContent.innerText =  cardDta[0]["description"]
        document.getElementById("displayDta").appendChild(dispContent)
        document.getElementById("displayDta").innerHTML += "<br><br>"
    
        let dispTagHold = document.createElement('p');
        dispTagHold.innerText += "Tags: "
    
        for(let f = 0; f<cardDta[0]["tags"].length; f++){
            if(f === 0){
                dispTagHold.innerText += cardDta[0]["tags"][f]
            }
            else{
                dispTagHold.innerText += ", " + cardDta[0]["tags"][f]
            }

            
           
        }

        let schoolHold = document.createElement('p');
        schoolHold.innerText += "School: "

        if(cardDta[0]["school"] === "THS"){
            schoolHold.innerText += "Troy High"
        }
        else if(cardDta[0]["school"] === "C"){
            schoolHold.innerText += "Cranbrook"
        }
        else if(cardDta[0]["school"] === "A"){
            schoolHold.innerText += "Rochester Adams"
        }

        
        document.getElementById("displayDta").appendChild(dispTagHold)
        document.getElementById("displayDta").appendChild(schoolHold)
        document.getElementById("displayDta").innerHTML += "<br><br>"
        let continueButton = document.createElement("input")
        continueButton.setAttribute("type", "submit")
        continueButton.setAttribute("class", "btn")
        continueButton.setAttribute("value", "Edit")
        continueButton.setAttribute("onclick", "editCatalog()")
        document.getElementById("displayDta").appendChild(continueButton)

        let deleteButton = document.createElement("input")
        deleteButton.setAttribute("type", "submit")
        deleteButton.setAttribute("class", "btn")
        deleteButton.setAttribute("style", "margin-left: 20px; ")
        deleteButton.setAttribute("value", "Delete")
        deleteButton.setAttribute("onclick", "deleteData()")
        document.getElementById("displayDta").appendChild(deleteButton)

        let queryButton = document.createElement("input")
        queryButton.setAttribute("type", "submit")
        queryButton.setAttribute("class", "btn")
        queryButton.setAttribute("style", "margin-left: 20px; ")
        queryButton.setAttribute("value", "Check Again")
        queryButton.setAttribute("onclick", "checkAgain()")
        document.getElementById("displayDta").appendChild(queryButton)

       
    }

    
    
}

function returnBack(){
    document.getElementById("finderForm").style.display = "block";

    document.getElementById("displayDta").innerHTML = ""
}

function checkAgain(){
    document.getElementById("displayDta").innerHTML = ""
    displayFoundData(cardDataStore.splice(1))
    
}
function deleteData(){
    document.getElementById("displayDta").innerHTML = ""
    if(typeOfClass === "course"){
        postData(databaseLink + '/deletecourse', cardDataStore[0])
        .then(data => {
            returnBack()
        });
    }
    else if(typeOfClass === "extracurricular"){
        postData(databaseLink + '/deleteextracurricular', cardDataStore[0])
        .then(data => {
            returnBack()
        });
    }
    else{
        postData(databaseLink + '/deleteclub', cardDataStore[0])
        .then(data => {
            returnBack()
        });
    }
    
}

function sendEdits(){

    var categories;
    var content;
    var title;
    var link;
    var tags = [];

    document.getElementById("changeBtnAct").disabled = true;
    document.getElementById("changeBtnAct").setAttribute("style", "background-color:#696969");


    categories = document.getElementById("CategoryCH").value.trim();

    



    content = document.getElementById("DescriptionCH").value.trim();
    title = document.getElementById("TitleCH").value.trim();
    link = document.getElementById("LinkCH").value.trim();
    let imageChange = document.getElementById("ImageCH").value.trim();

    for(let u = 0; u < parseInt(tgCH.value.trim()); u++){
        tags.push(document.getElementById("tg" + u).value.trim())
    }
    hrs = document.getElementById("Hours CommitmentCH").value.trim();
    cost = document.getElementById("CostCH").value.trim();


    let changedObj = cardDataStore[0]
    for(let i = 0; i < allComponents.length; i++){
        if(selectedComponents.includes(allComponents[i])){
            if(allComponents[i] === "Title"){
                changedObj[objVals[i]] = title;
            }
            else if(allComponents[i] === "Image"){
                changedObj[objVals[i]] = imageChange;
            }
            else if(allComponents[i] === "Category"){
                changedObj[objVals[i]] = categories;
            }
            else if(allComponents[i] === "Description"){
                changedObj[objVals[i]] = content;
            }
            else if(allComponents[i] === "Link"){
                changedObj[objVals[i]] = link;
            }
            else if(allComponents[i] === "Tags"){
                changedObj[objVals[i]] = tags;
            }
            else if(allComponents[i] === "Hours Commitment"){
                changedObj[objVals[i]] = hrs;
            }
            else if(allComponents[i] === "Cost"){
                changedObj[objVals[i]] = cost;
            }
        }
    }

    if(typeOfClass === "course"){
        postData(databaseLink + '/savecourse', changedObj)
        .then(data => {
            if(data["status"] != "200"){
                alert("Error: " + data["status"])
            }
            document.getElementById("ImageCH").value = "";
            document.getElementById("DescriptionCH").value = "";
            document.getElementById("TitleCH").value = "";
            document.getElementById("LinkCH").value = "";
            document.getElementById("Hours CommitmentCH").value = "";
            document.getElementById("CostCH").value = "";
            document.getElementById("CategoryCH").selectedIndex = 0;
            document.getElementById("tgsChange").selectedIndex = 0;
    
            changeCategoryCH()
    
            document.getElementById("changeBtnAct").disabled = false;
            document.getElementById("changeBtnAct").setAttribute("style", "");
            document.getElementById("editorForm").style.display = "none"
            returnBack();
        });
    }
    else if(typeOfClass === "extracurricular"){
        postData(databaseLink + '/saveextracurriculars', changedObj)
        .then(data => {
            if(data["status"] != "200"){
                alert("Error: " + data["status"])
            }
            document.getElementById("ImageCH").value = "";
            document.getElementById("DescriptionCH").value = "";
            document.getElementById("TitleCH").value = "";
            document.getElementById("LinkCH").value = "";
            document.getElementById("Hours CommitmentCH").value = "";
            document.getElementById("CostCH").value = "";
            document.getElementById("CategoryCH").selectedIndex = 0;
            document.getElementById("tgsChange").selectedIndex = 0;
    
            changeCategoryCH()
    
            document.getElementById("changeBtnAct").disabled = false;
            document.getElementById("changeBtnAct").setAttribute("style", "");
            document.getElementById("editorForm").style.display = "none"
            returnBack();
        });
    }
    else{
        postData(databaseLink + '/saveclub', changedObj)
        .then(data => {
            if(data["status"] != "200"){
                alert("Error: " + data["status"])
            }
            document.getElementById("ImageCH").value = "";
            document.getElementById("DescriptionCH").value = "";
            document.getElementById("TitleCH").value = "";
            document.getElementById("LinkCH").value = "";
            document.getElementById("Hours CommitmentCH").value = "";
            document.getElementById("CostCH").value = "";
            document.getElementById("CategoryCH").selectedIndex = 0;
            document.getElementById("tgsChange").selectedIndex = 0;
    
            changeCategoryCH()
    
            document.getElementById("changeBtnAct").disabled = false;
            document.getElementById("changeBtnAct").setAttribute("style", "");
            document.getElementById("editorForm").style.display = "none"
            returnBack();
           
        });
    }
}

function editCatalog(){
    document.getElementById("displayDta").innerHTML = "";
    document.getElementById("editorForm").style.display = "block";
}

var Carousel;
var counter = 0;
var firstPromiseCar = fetch(databaseLink + "/getCarousel");
var secondPromiseCar = firstPromiseCar.then(response => response.json());
secondPromiseCar.then(cardDta => {
    Carousel = cardDta;
    document.getElementById("CarouselImg").setAttribute("src", Carousel[0]["img"])
});



function contCar(){
    counter++;
    document.getElementById("CarouselImg").setAttribute("src", Carousel[counter % Carousel.length]["img"])
    
}

function deleteCar(){
    document.getElementById("carDlt").disabled = true;
    document.getElementById("carDlt").setAttribute("style", "background-color:#696969");;

    postData(databaseLink + '/deleteCarousel', Carousel[counter % Carousel.length])
    .then(data => {
        document.getElementById("carDlt").disabled = false;
        document.getElementById("carDlt").setAttribute("style", "");;

        location.reload();
    });

}









// Edit Select info

$('#multSelect').on('change', function (e) {
    selectedComponents = []
    for(let i = 0; i < $('#multSelect').select2('data').length; i++){
        selectedComponents.push($('#multSelect').select2('data')[i]["text"])

        
    }

    for(let i = 0; i < allComponents.length; i++){
        if(selectedComponents.includes(allComponents[i])){
            document.getElementById(allComponents[i]).style.display = "block"
        }
        else{
            document.getElementById(allComponents[i]).style.display = "none"
        }
    }

    if(selectedComponents.length != 0){
        document.getElementById("changeBtn").style.display = "block"
    }
    else{
        document.getElementById("changeBtn").style.display = "none"
    }
    
  });



  var comments = [];
  var approvedComments = [];
  var deniedComments = [];

  var clubcomments = [];
  var approvedClubComments = [];
  var deniedClubComments = [];

  var ccoments = [];
  var cclubcomments = [];
  var tableInp = document.getElementById("table");




  var Clubs;
  var Courses;
  var Extracurriculars;

  var names = [];

  var tally = 0;
  var tally2 = 0;

  var firstPromiseComments1 = fetch(databaseLink + "/getclubs");
  var secondPromiseComments2 = firstPromiseComments1.then(response => response.json());
      secondPromiseComments2.then(commentsDtaClub => {
          Clubs = commentsDtaClub;
     
          for(let i = 0; i < commentsDtaClub.length; i++){
              for(let x = 0; x < commentsDtaClub[i]["commentsUnapproved"].length; x++){
                  let innerCont = document.createElement("tr");
                  let com = document.createElement("td");
                  com.innerText = commentsDtaClub[i]["commentsUnapproved"][x];
                  innerCont.appendChild(com);

                  tableInp.appendChild(innerCont); 

                  names.push(commentsDtaClub[i])

              }
              
      }

      var firstPromiseComments = fetch(databaseLink + "/getcourse");
      var secondPromiseComments = firstPromiseComments.then(response => response.json());
      secondPromiseComments.then(commentsDta => {
          Courses = commentsDta;
          for(let i = 0; i < commentsDta.length; i++){
              for(let x = 0; x < commentsDta[i]["commentsUnapproved"].length; x++){
                  let innerCont = document.createElement("tr");
                  let com = document.createElement("td");
                  com.innerText = commentsDta[i]["commentsUnapproved"][x];
                  innerCont.appendChild(com);

                  tableInp.appendChild(innerCont); 

                  names.push(commentsDta[i])

              }

          }
      });

      var firstPromiseComments2 = fetch(databaseLink + "/getextracurriculars");
      var secondPromiseComments2 = firstPromiseComments2.then(response => response.json());
      secondPromiseComments2.then(commentsDta => {
          Extracurriculars = commentsDta;
          for(let i = 0; i < commentsDta.length; i++){
              for(let x = 0; x < commentsDta[i]["commentsUnapproved"].length; x++){
                  let innerCont = document.createElement("tr");
                  let com = document.createElement("td");
                  com.innerText = commentsDta[i]["commentsUnapproved"][x];
                  innerCont.appendChild(com);

                  tableInp.appendChild(innerCont); 

                  names.push(commentsDta[i])

              }

          }
      });

  });

  function deny(){
    console.log(names[tally])
    names[tally]["commentsUnapproved"].pop( tableInp.rows[0].innerText)
    if(names[tally].hasOwnProperty("city")){
        postData(databaseLink + '/saveextracurriculars', names[tally])
    }
    else if(names[tally].hasOwnProperty("avgRating")){
        postData(databaseLink + '/saveclub', names[tally])
    }
    
    else{
        postData(databaseLink + '/savecourse', names[tally])
    }

    
    tableInp.deleteRow(0);
    tally++;


}
function approve(){
    names[tally]["commentsUnapproved"].pop( tableInp.rows[0].innerText)
    names[tally]["comments"].push(tableInp.rows[0].innerText)
    if(names[tally].hasOwnProperty("city")){
        postData(databaseLink + '/saveextracurriculars', names[tally])
    }
    else if(names[tally].hasOwnProperty("avgRating")){
        postData(databaseLink + '/saveclub', names[tally])
    }
    
    else{
        postData(databaseLink + '/savecourse', names[tally])
    }



    tableInp.deleteRow(0);
    tally++;



}

// replace width > max-width; add width: 100%; height: auto;

function articleSubmit(){
    document.getElementById("articleBtn").disabled = true;
    document.getElementById("articleBtn").setAttribute("style", "background-color:#696969");;

    var expObj = {}


    if(document.getElementById("articleTitle").value.trim() === "" || document.getElementById("articleTitle").value === undefined){
        alert("Complete the input")
        document.getElementById("articleBtn").disabled = false;
        document.getElementById("articleBtn").setAttribute("style", "");
        return
    }
    else if(document.getElementById("articleTtlImg").value.trim() === "" || document.getElementById("articleTtlImg").value === undefined){
        alert("Complete the input")
        document.getElementById("articleBtn").disabled = false;
        document.getElementById("articleBtn").setAttribute("style", ""); 
        return  }
    else if(document.getElementById("hexcolor").value.trim() === "" || document.getElementById("hexcolor").value === undefined){
        document.getElementById("hexcolor").value === "#FFFFFF"
          }
    else if(document.getElementById("articleDate").value.trim() === "" || document.getElementById("articleDate").value === undefined){
        alert("Complete the input")
        document.getElementById("articleBtn").disabled = false;
        document.getElementById("articleBtn").setAttribute("style", ""); 
        return  }
    else if(document.getElementById("author").value.trim() === "" || document.getElementById("author").value === undefined){
        var auth = "Anonymous"
    }
    else{
        var auth = document.getElementById("author").value;
    }
    if($('#summernote').summernote('isEmpty')){
        alert("Article content is empty")
        document.getElementById("articleBtn").disabled = false;
        document.getElementById("articleBtn").setAttribute("style", "");
    }
    else{
        expObj["author"] = auth
        expObj["color"] = document.getElementById("hexcolor").value
        expObj["category"] = document.getElementById("articleCat").value
       // expObj["content"] = $('#summernote').summernote('code').replaceAll(/(<img\ssrc=".*"\sstyle=")(width)(:\s\d*px;)/g, '$1' + 'max-width' + '$3' + 'width: 100%; height: auto;');
        expObj["content"] = $('#summernote').summernote('code').replaceAll(/style="width: (\d+\.*\d*px);"/g, 'style = "' +  'max-width: $1; width: 100%; height: auto;"');
  
        
        
        expObj["title"] = document.getElementById("articleTitle").value
        expObj["date"] = document.getElementById("articleDate").value
        expObj["titleImage"] = document.getElementById("articleTtlImg").value
        postData(databaseLink + '/savearticle', expObj).then(data => {
            if(data["status"] != "200"){
                alert("Error: " + data["status"])
            }
            document.getElementById("articleBtn").disabled = false;
            document.getElementById("author").value = "";
            document.getElementById("articleCat").selectedIndex = 0;
            document.getElementById("hexcolor").value = "";
            document.getElementById("articleTitle").value = ""
            document.getElementById("articleDate").value = ""
            document.getElementById("articleTtlImg").value = ""
            document.getElementById("articleBtn").setAttribute("style", "");
            $('#summernote').summernote('reset');
        })
    }
    
}

function findArticle(){
    let authorName = document.getElementById("authorName").value
    let articleTitle = document.getElementById("articleTitle").value
    let articleCat = document.getElementById("typeArticle").value
    document.getElementById("findArticleBtn").disabled = true;
    document.getElementById("findArticleBtn").setAttribute("style", "background-color:#696969");


    let foundArticle = [];

    var firstPromiseArt = fetch(databaseLink + "/getarticles");
    var secondPromiseArt = firstPromiseArt.then(response => response.json());
    secondPromiseArt.then(artDta => {
        for(let i = 0; i< artDta.length; i++){
            if(artDta[i]["author"] === authorName && artDta[i]["title"] === articleTitle && artDta[i]["category"] === articleCat){
                foundArticle.push(artDta[i])
            }
        }


        document.getElementById("findArticleBtn").disabled = false;
        document.getElementById("findArticleBtn").setAttribute("style", "");

    });

}

function submitCarousel(){
    exportObj = {}
    carouselImg = document.getElementById("carouselImg").value.trim()
    originURL = document.getElementById("originURL").value.trim()
    exportObj["img"] = carouselImg;
    exportObj["originURL"] = originURL;

    signupURL = document.getElementById("signupURL").value.trim()
    opIMG = document.getElementById("opImg").value.trim()
    if( document.getElementById("decideLanding").selectedIndex === 0){
        exportObj["signupURL"] = signupURL;
        exportObj["opImg"] = opIMG;
    }

    document.getElementById("carBtn").disabled = true;
    document.getElementById("carBtn").setAttribute("style", "background-color:#696969");;

    postData(databaseLink + '/saveCarousel', exportObj)
    .then(data => {
        if(data["status"] != "200"){
            alert("Error: " + data["status"])
        }
        carouselImg = document.getElementById("carouselImg").value = ""
        carouselImg = document.getElementById("opImg").value = ""
        carouselURL = document.getElementById("signupURL").value = ""
        carouselURL = document.getElementById("originURL").value = ""
        document.getElementById("decideLanding").selectedIndex = 0
        document.getElementById("carBtn").disabled = false;
        document.getElementById("carBtn").setAttribute("style", "");;
        document.getElementById("opImg").disabled = false;
        document.getElementById("opImg").required = true;
        document.getElementById("signupURL").disabled = false;
        document.getElementById("signupURL").required = true;
    });


  
}


async function postData(url = '', data = {}) {
    // Default options are marked with *
    const response = await fetch(url, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        headers: {
        'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: JSON.stringify(data) // body data type must match "Content-Type" header
    });
    
    return response; // parses JSON response into native JavaScript objects
}

document.getElementById("decideLanding").onchange = function() {
    if(document.getElementById("decideLanding").value == "land"){
        document.getElementById("opImg").disabled = false;
        document.getElementById("opImg").required = true;
        document.getElementById("signupURL").disabled = false;
        document.getElementById("signupURL").required = true;
    }
    else{
        document.getElementById("opImg").disabled = true;
        document.getElementById("opImg").required = false;
        document.getElementById("signupURL").disabled = true;
        document.getElementById("signupURL").required = false;
    }
}


